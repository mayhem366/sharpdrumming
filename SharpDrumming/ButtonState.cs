﻿namespace SharpDrumming
{
    public class ButtonState
    {
        public const int Unpressed = 0;
        public const int Pressed = 128;
    }
}
