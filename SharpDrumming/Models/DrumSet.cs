﻿using System;
using System.Collections.Generic;

namespace SharpDrumming
{
    [Serializable]
    public class DrumSet
    {
        private readonly List<Drum> m_drums = new List<Drum>();

        public string Name { get; set; }

        public List<Drum> Drums
        {
            get { return m_drums; }
        }
    }
}
