﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using SharpDX.DirectInput;

namespace SharpDrumming.UnitTests
{
    class InputChordListenerTests
    {
        //[SetUp]
        //public void TestFixtureSetup()
        //{
        //    RxMonitoring.Init();
        //    RxMonitoring.WaitForReady();
        //    RxMonitoring.Enable();
        //}

        [Test]
        public void ChordPressed_InputPollerFiresStateChangedEventForOnlyOneItemInChord_DoesNotFireChordPressedEvent()
        {
            bool eventFired = false;
            var inputPollerMock = new Mock<IInputPoller>();
            var chordListener = new InputChordListener(inputPollerMock.Object, new List<JoystickOffset>
            {
                JoystickOffset.Buttons1,
                JoystickOffset.Buttons2
            });
            chordListener.ChordPressed += (sender, args) => eventFired = true;
            var joystickUpdates = new List<JoystickUpdate>
            {
                new JoystickUpdate {RawOffset = (int)JoystickOffset.Buttons1, Value = ButtonState.Pressed}
            };

            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdates));

            Assert.IsFalse(eventFired);
        }

        [Test]
        public void ChordPressed_InputPollerFiresStateChangedEventForSingleItemsInChord_FiresChordPressedEvent()
        {
            bool eventFired = false;
            var inputPollerMock = new Mock<IInputPoller>();
            var chordListener = new InputChordListener(inputPollerMock.Object, new List<JoystickOffset>
            {
                JoystickOffset.Buttons1
            });
            chordListener.ChordPressed += (sender, args) => eventFired = true;
            var joystickUpdates = new List<JoystickUpdate>
            {
                new JoystickUpdate {RawOffset = (int)JoystickOffset.Buttons1, Value = ButtonState.Pressed}
            };

            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdates));

            Assert.IsTrue(eventFired);
        }

        [Test]
        public void ChordPressed_InputPollerFiresStateChangedEventForMultipleItemsInChord_FiresChordPressedEvent()
        {
            bool eventFired = false;
            var inputPollerMock = new Mock<IInputPoller>();
            var chordListener = new InputChordListener(inputPollerMock.Object, new List<JoystickOffset>
            {
                JoystickOffset.Buttons1,
                JoystickOffset.Buttons2
            });
            chordListener.ChordPressed += (sender, args) => eventFired = true;
            var joystickUpdates = new List<JoystickUpdate>
            {
                new JoystickUpdate {RawOffset = (int)JoystickOffset.Buttons1, Value = ButtonState.Pressed},
                new JoystickUpdate {RawOffset = (int)JoystickOffset.Buttons2, Value = ButtonState.Pressed},
            };

            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdates));

            Assert.IsTrue(eventFired);
        }

        [Test]
        public void ChordReleased_InputPollerFiresStateChangedEventWithAllItemsPressedAndThenReleased_FiresChordReleasedEvent()
        {
            bool eventFired = false;
            var inputPollerMock = new Mock<IInputPoller>();
            var chordListener = new InputChordListener(inputPollerMock.Object, new List<JoystickOffset>
            {
                JoystickOffset.Buttons1
            });
            chordListener.ChordReleased += (sender, args) => eventFired = true;
            var joystickUpdateDown = new List<JoystickUpdate> { new JoystickUpdate { RawOffset = (int)JoystickOffset.Buttons1, Value = ButtonState.Pressed } };
            var joystickUpdateUp = new List<JoystickUpdate> { new JoystickUpdate { RawOffset = (int)JoystickOffset.Buttons1, Value = ButtonState.Unpressed } };

            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateDown));
            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateUp));

            Assert.IsTrue(eventFired);
        }

        [Test]
        public void ChordReleased_InputPollerFiresMultipleStateChangedEventsWithSameInput_ChordReleasedAfterSingleElementNoLongerDown()
        {
            bool released = false;
            var inputPollerMock = new Mock<IInputPoller>();
            var chordListener = new InputChordListener(inputPollerMock.Object, new List<JoystickOffset>
            {
                JoystickOffset.Buttons1, JoystickOffset.Buttons2
            });
            chordListener.ChordReleased += (sender, args) => released = true;
            var joystickUpdateDown = new List<JoystickUpdate> { new JoystickUpdate { RawOffset = (int)JoystickOffset.Buttons1, Value = ButtonState.Pressed } };
            var joystickUpdateDown2 = new List<JoystickUpdate> { new JoystickUpdate { RawOffset = (int)JoystickOffset.Buttons2, Value = ButtonState.Pressed } };
            var joystickUpdateUp = new List<JoystickUpdate> { new JoystickUpdate { RawOffset = (int)JoystickOffset.Buttons1, Value = ButtonState.Unpressed } };

            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateDown));
            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateDown2));
            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateUp));

            Assert.IsTrue(released);
        }

        [Test]
        public void ChordReleased_InputPollerFiresMultipleStateChangedEventsWithSameInput_ChordReleasedIsOnlyFiredOnce()
        {
            int timesReleased = 0;
            var inputPollerMock = new Mock<IInputPoller>();
            var chordListener = new InputChordListener(inputPollerMock.Object, new List<JoystickOffset>
            {
                JoystickOffset.Buttons1, JoystickOffset.Buttons2
            });
            chordListener.ChordReleased += (sender, args) => ++timesReleased;
            var joystickUpdateDown = new List<JoystickUpdate> { new JoystickUpdate { RawOffset = (int)JoystickOffset.Buttons1, Value = ButtonState.Pressed } };
            var joystickUpdateDown2 = new List<JoystickUpdate> { new JoystickUpdate { RawOffset = (int)JoystickOffset.Buttons2, Value = ButtonState.Pressed } };
            var joystickUpdateUp = new List<JoystickUpdate> { new JoystickUpdate { RawOffset = (int)JoystickOffset.Buttons1, Value = ButtonState.Unpressed } };
            var joystickUpdateUp2 = new List<JoystickUpdate> { new JoystickUpdate { RawOffset = (int)JoystickOffset.Buttons2, Value = ButtonState.Unpressed } };

            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateDown));
            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateDown2));
            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateUp));
            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateUp2));

            Assert.AreEqual(1, timesReleased);
        }

        [Test]
        public void ChordReleased_InputPollerFiresMultipleDownEventsForBothElementsInChord_ChordReleasedIsNotFired()
        {
            bool released = false;
            var inputPollerMock = new Mock<IInputPoller>();
            var chordListener = new InputChordListener(inputPollerMock.Object, new List<JoystickOffset>
            {
                JoystickOffset.Buttons1, JoystickOffset.Buttons2
            });
            chordListener.ChordReleased += (sender, args) => released = true;
            var joystickUpdateDown = new List<JoystickUpdate> { new JoystickUpdate { RawOffset = (int)JoystickOffset.Buttons1, Value = ButtonState.Pressed } };
            var joystickUpdateDown2 = new List<JoystickUpdate> { new JoystickUpdate { RawOffset = (int)JoystickOffset.Buttons2, Value = ButtonState.Pressed } };

            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateDown));
            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateDown2));

            Assert.IsFalse(released);
        }

        [Test]
        public void ChordPressed_InputPollerFiresMultipleStateChangedEventsWithSameInput_ChordPressedIsOnlyFiredOnce()
        {
            int timesEventFired = 0;
            var inputPollerMock = new Mock<IInputPoller>();
            var chordListener = new InputChordListener(inputPollerMock.Object, new List<JoystickOffset>
            {
                JoystickOffset.Buttons1
            });
            chordListener.ChordPressed += (sender, args) => timesEventFired++;
            var joystickUpdateDown = new List<JoystickUpdate> { new JoystickUpdate { RawOffset = (int)JoystickOffset.Buttons1, Value = ButtonState.Pressed } };

            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateDown));
            inputPollerMock.Raise(poller => poller.ButtonStateChanged += null, new StateChangedEventArgs(joystickUpdateDown));

            Assert.AreEqual(1, timesEventFired);
        }
    }
}
