﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;

namespace SharpDrumming.UnitTests
{
    public class ChordBlockerTests
    {
        [Test]
        public void ChordPressed_SingleChordIsPressed_FiresChordPressedEvent()
        {
            var chordListener = new Mock<IChordListener>();
            var chordBlocker = new ChordBlocker(new[] { chordListener.Object });
            bool eventFired = false;
            chordBlocker.ChordPressed += (sender, args) => eventFired = true;

            chordListener.Raise(listener => listener.ChordPressed += null, EventArgs.Empty);

            Assert.IsTrue(eventFired);
        }

        [Test]
        public void ChordPressed_SingleChordIsPressed_ChordPressedEventHasChordListenerAsSender()
        {
            var chordListener = new Mock<IChordListener>();
            var chordBlocker = new ChordBlocker(new[] { chordListener.Object });
            object eventSender = null;
            chordBlocker.ChordPressed += (sender, args) => eventSender = sender;

            chordListener.Raise(listener => listener.ChordPressed += null, EventArgs.Empty);

            Assert.AreSame(chordListener.Object, eventSender);
        }

        [Test]
        public void ChordPressed_ChordAlreadyPressedAndSecondChordIsPressed_DoesNotFireChordPressedEventForSecondChord()
        {
            var chordListener1 = new Mock<IChordListener>();
            var chordListener2 = new Mock<IChordListener>();
            var chordBlocker = new ChordBlocker(new[] { chordListener1.Object, chordListener2.Object });
            var eventSenders = new List<object>();
            chordBlocker.ChordPressed += (sender, args) => eventSenders.Add(sender);

            chordListener1.Raise(listener => listener.ChordPressed += null, EventArgs.Empty);
            chordListener2.Raise(listener => listener.ChordPressed += null, EventArgs.Empty);

            CollectionAssert.DoesNotContain(eventSenders, chordListener2.Object);
        }

        [Test]
        public void ChordPressed_SecondChordIsPressedOnly_FiresChordPressedEventForSecondChord()
        {
            var chordListener1 = new Mock<IChordListener>();
            var chordListener2 = new Mock<IChordListener>();
            var chordBlocker = new ChordBlocker(new[] { chordListener1.Object, chordListener2.Object });
            var eventSenders = new List<object>();
            chordBlocker.ChordPressed += (sender, args) => eventSenders.Add(sender);

            chordListener2.Raise(listener => listener.ChordPressed += null, EventArgs.Empty);

            CollectionAssert.Contains(eventSenders, chordListener2.Object);
        }

        [Test]
        public void ChordPressed_ChordAlreadyPressedAndThenReleasedAndSecondChordIsPressed_FiresChordPressedEventForSecondChord()
        {
            var chordListener1 = new Mock<IChordListener>();
            var chordListener2 = new Mock<IChordListener>();
            var chordBlocker = new ChordBlocker(new[] { chordListener1.Object, chordListener2.Object });
            var eventSenders = new List<object>();
            chordBlocker.ChordPressed += (sender, args) => eventSenders.Add(sender);

            chordListener1.Raise(listener => listener.ChordPressed += null, EventArgs.Empty);
            chordListener1.Raise(listener => listener.ChordReleased += null, EventArgs.Empty);
            chordListener2.Raise(listener => listener.ChordPressed += null, EventArgs.Empty);

            CollectionAssert.Contains(eventSenders, chordListener2.Object);
        }

        [Test]
        public void ChordReleased_SingleChordPressedAndReleased_FiresChordReleased()
        {
            var chordListener = new Mock<IChordListener>();
            var chordBlocker = new ChordBlocker(new[] { chordListener.Object });
            bool eventFired = false;
            chordBlocker.ChordReleased += (sender, args) => eventFired = true;

            chordListener.Raise(listener => listener.ChordPressed += null, EventArgs.Empty);
            chordListener.Raise(listener => listener.ChordReleased += null, EventArgs.Empty);

            Assert.IsTrue(eventFired);
        }

        [Test]
        public void ChordReleased_ChordAlreadyPressedAndSecondChordPressedAndReleased_DoesNotFireChordReleased()
        {
            var chordListener1 = new Mock<IChordListener>();
            var chordListener2 = new Mock<IChordListener>();
            var chordBlocker = new ChordBlocker(new[] { chordListener1.Object, chordListener2.Object });
            bool eventFired = false;
            chordBlocker.ChordReleased += (sender, args) => eventFired = true;

            chordListener1.Raise(listener => listener.ChordPressed += null, EventArgs.Empty);
            chordListener2.Raise(listener => listener.ChordPressed += null, EventArgs.Empty);
            chordListener2.Raise(listener => listener.ChordReleased += null, EventArgs.Empty);

            Assert.IsFalse(eventFired);
        }

        [Test]
        public void Dispose_GivenAChordListener_DiposesOfThatChordListener()
        {
            var chordListener = new Mock<IChordListener>();
            var chordBlocker = new ChordBlocker(new[] { chordListener.Object });

            chordBlocker.Dispose();

            chordListener.Verify(c => c.Dispose());
        }
    }
}