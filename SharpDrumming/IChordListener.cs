﻿using System;

namespace SharpDrumming
{
    public interface IChordListener : IDisposable
    {
        event EventHandler ChordPressed;
        event EventHandler ChordReleased;
    }
}